#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main()
{
    int fd, salida;
    char buffer[256];
    ssize_t tamanio_mensaje;
    char mensaje[] = "Sistemas Operativos 2020\n";
    tamanio_mensaje = strlen(mensaje);
    char ruta[] = "mensaje_a_imprimir_en_pantalla";

    fd = open(ruta, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    salida = write(fd, mensaje, tamanio_mensaje);
    close(fd);

    fd = open(ruta, O_RDONLY);
    salida = read(fd, buffer, salida);
    salida = write(1, buffer, salida);

    close(fd);
    return 0;
}
