#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

int main(){

    int tamanio_buffer = 256;
    int origen, entrada;
    char buf[tamanio_buffer];

    origen = open("origen.txt", O_RDONLY);

    if (origen < 0){
        syscall(SYS_write, 1, "Error Con el archivo", 20);
    }else
    {
        entrada = read(origen, buf, tamanio_buffer);
        syscall(SYS_write, 1, buf, strlen(buf));
    }
    
    close(origen);
    
    return 0;
}
