#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

int main(){

    char entrada[31];

    syscall(SYS_write, 1, "Ingrese una frase corta: ", 30);
    int nread = read(0, entrada, sizeof entrada -1);
    entrada[nread-1] = '\0';

    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    syscall(SYS_write, 1, entrada, strlen(entrada));
    syscall(SYS_write, 1, "\n", 1);
    
    
    return 0;
}
